using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public float startTime = 3.0f;
    public int score = 0;
    public float lifeTime = 1.0f;
    public int hitBlockScore = 10;
    public float missBlockLife = 0.1f;
    public float wrongBlockLife = 0.08f;
    public float lifeRegenrate = 0.1f;

    //instance
    public static GameManager instance;

    void Awake()
    {
        instance = this;
    }

    public void AddScore()
    {
        score += hitBlockScore;
        GameUi.instance.updateScoreText();
    }

    public void MissBlock()
    {
        lifeTime -= missBlockLife;
    }

    public void HitWrongBlock()
    {
        lifeTime -= wrongBlockLife;
    }

    void Update()
    {
        lifeTime = Mathf.MoveTowards(lifeTime, 1.0f, lifeRegenrate * Time.deltaTime);
        GameUi.instance.UpdateLifetimeBar();
    }
}

