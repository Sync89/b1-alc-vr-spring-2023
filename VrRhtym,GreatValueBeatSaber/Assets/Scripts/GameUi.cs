using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameUi : MonoBehaviour
{
    public TextMeshPro scoreText;
    public Image lifetimeBar;

    public static GameUi instance;

    void Awake()
    {
        instance = this;
    }

    public void updateScoreText()
    {
        scoreText.text = string.Format("SCORE\n{0}", GameManager.instance.score.ToString());
    }

    public void UpdateLifetimeBar()
    {
        lifetimeBar.fillAmount = GameManager.instance.lifeTime;
    }
}
